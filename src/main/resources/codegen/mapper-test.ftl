package ${pkg}.mappers;

import java.util.Date;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ${pkg}.mappers.${ModelName}Mapper;
import ${pkg}.models.${ModelName};

${objDoc}
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/dao.xml")
public class ${ModelName}MapperTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private ${ModelName}Mapper mapper;

	@Test
	public void mapperTest() {
		String id = String.valueOf(new Date().getTime());
        // init model
		${ModelName} m = new ${ModelName}();
		m.setId(id);
		m.setDateCreated(new Date());
    <#list cols as prop>
    <#if prop.javaType == 'Date'>
		m.set${prop.Name}(new Date());
    <#elseif prop.javaType == 'String'>
		m.set${prop.Name}("s");
    <#elseif prop.javaType == 'boolean'>
		m.set${prop.Name}(false);
    <#elseif prop.javaType == 'int'>
		m.set${prop.Name}(1);
    <#elseif prop.javaType == 'long'>
		m.set${prop.Name}(1L);
    <#elseif prop.javaType == 'float'>
		m.set${prop.Name}(1.0F);
    <#elseif prop.javaType == 'double'>
		m.set${prop.Name}(1.0D);
    </#if>
    </#list>
		mapper.insert(m);
		${ModelName} m1 = mapper.find(id);
        // asserts
		Assert.assertEquals(id, m1.getId());
    <#list cols as prop>
    <#if prop.javaType == 'String'>
		Assert.assertEquals("s", m.get${prop.Name}());
    <#elseif prop.javaType == 'boolean'>
		Assert.assertEquals(false, m.is${prop.Name}());
    <#elseif prop.javaType == 'int'>
		Assert.assertEquals(1, m.get${prop.Name}());
    <#elseif prop.javaType == 'long'>
		Assert.assertEquals(1L, m.get${prop.Name}());
    <#elseif prop.javaType == 'float'>
		Assert.assertEquals(1.0F, m.get${prop.Name}());
    <#elseif prop.javaType == 'double'>
		Assert.assertEquals(1.0D, m.get${prop.Name}());
    </#if>
    </#list>
        // update
		m.setDateUpdated(new Date());
    <#list cols as prop>
    <#if prop.javaType == 'Date'>
		m.set${prop.Name}(new Date());
    <#elseif prop.javaType == 'String'>
		m.set${prop.Name}("S");
    <#elseif prop.javaType == 'boolean'>
		m.set${prop.Name}(true);
    <#elseif prop.javaType == 'int'>
		m.set${prop.Name}(2);
    <#elseif prop.javaType == 'long'>
		m.set${prop.Name}(2L);
    <#elseif prop.javaType == 'float'>
		m.set${prop.Name}(2.0F);
    <#elseif prop.javaType == 'double'>
		m.set${prop.Name}(2.0D);
    </#if>
    </#list>
		mapper.update(m);
		m1 = mapper.find(id);
        // asserts
		Assert.assertEquals(id, m1.getId());
		Assert.assertNotNull(m1.getDateUpdated());
    <#list cols as prop>
    <#if prop.javaType == 'String'>
		Assert.assertEquals("S", m.get${prop.Name}());
    <#elseif prop.javaType == 'boolean'>
		Assert.assertEquals(true, m.is${prop.Name}());
    <#elseif prop.javaType == 'int'>
		Assert.assertEquals(2, m.get${prop.Name}());
    <#elseif prop.javaType == 'long'>
		Assert.assertEquals(2L, m.get${prop.Name}());
    <#elseif prop.javaType == 'float'>
		Assert.assertEquals(2.0F, m.get${prop.Name}());
    <#elseif prop.javaType == 'double'>
		Assert.assertEquals(2.0D, m.get${prop.Name}());
    </#if>
    </#list>
        // list
		Assert.assertTrue(mapper.list(null).size() > 0);
		// delete
		mapper.delete(id);
		m1 = mapper.find(id);
		Assert.assertNull(m1);
	}
}
