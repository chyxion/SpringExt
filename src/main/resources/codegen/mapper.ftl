package ${pkg}.mappers;

import ${baseMapperFullName};
import ${pkg}.models.${ModelName};

${objDoc}
public interface ${ModelName}Mapper extends ${baseMapperName}<${ModelName}> {

}
