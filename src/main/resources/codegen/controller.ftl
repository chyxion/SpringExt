package ${pkg}.controllers;

import java.util.Map;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import ${pkg}.models.${ModelName};
import ${pkg}.services.${ModelName}Service;
import com.alibaba.fastjson.JSONArray;

${objDoc}
@Controller
@RequestMapping("/${url}")
public class ${ModelName}Controller {

	@Autowired
	private ${ModelName}Service service;

	@RequestMapping("/list")
	public Map<String, Object> list(
	        @RequestParam(value = "start", defaultValue = "0") int start,
	        @RequestParam(value = "limit", defaultValue = "16") int limit,
	        @RequestParam(value = "sort", required = false) JSONArray sort) {
		return service.list(start, limit, sort);
	}

	@RequestMapping(value = "/{id}", method = GET)
    public ${ModelName} get(@PathVariable("id") String id) {
        return service.find(id);
	}

	@RequestMapping(method = POST)
    public ${ModelName} create(@Valid ${ModelName} ${modelName}) {
        return service.create(${modelName})[0];
	}

	@RequestMapping(method = PUT)
    public ${ModelName} update(@Valid ${ModelName} ${modelName}) {
        return service.update(${modelName})[0];
	}
	
	@RequestMapping(method = DELETE)
	public int delete(@RequestParam("id") JSONArray id) {
		return service.delete(id);
	}

	@RequestMapping(value = "/{id}", method = DELETE)
	public int delete(@PathVariable("id") String id) {
		return service.delete(id);
	}
}
