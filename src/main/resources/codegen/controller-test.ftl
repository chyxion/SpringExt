package ${pkg}.controllers;

import java.util.Map;
import java.util.HashMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ${ctrlrTestToolFullName};

${objDoc}
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/controller.xml")
public class ${ModelName}ControllerTest {

	@Autowired
	private ${ctrlrTestToolName} t;

	/**
	 * test list
	 */
	@Test
	public void testList() {
		// TODO Modify Mock${ModelName}Mapper#listPage To Complete Test Logic
		t.assertSuccess(t.print(t.get("/${url}/list")));
	}

	/**
	 * test create
	 */
	@Test
	public void testCreate() {
		// TODO Modify Mock${ModelName}Mapper#insert To Complete Test Logic
		Map<String, Object> params = new HashMap<String, Object>();
    <#list cols as prop>
    <#if prop.javaType == 'Date'>
		params.put("${prop.Name}", new Date());
    <#elseif prop.javaType == 'String'>
		params.put("${prop.Name}", "String Value");
    <#elseif prop.javaType == 'boolean'>
		params.put("${prop.Name}", false);
    <#elseif prop.javaType == 'int'>
		params.put("${prop.Name}", 1);
    <#elseif prop.javaType == 'long'>
		params.put("${prop.Name}", 1L);
    <#elseif prop.javaType == 'float'>
		params.put("${prop.Name}", 1.0F);
    <#elseif prop.javaType == 'double'>
		params.put("${prop.Name}", 1.0D);
    </#if>
    </#list>
		t.print(t.put("/${url}", params));
	} 

	/**
	 * test show
	 */
	@Test
	public void testShow() {
		// TODO Modify Mock${ModelName}Mapper#find To Complete Test Logic
		t.assertSuccess(t.print(t.get("/${url}/1")));
	} 

	/**
	 * test update
	 */
	@Test
	public void testUpdate() {
		// TODO Modify Mock${ModelName}Mapper#update To Complete Test Logic
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", 1);
    <#list cols as prop>
    <#if prop.javaType == 'Date'>
		params.put("${prop.Name}", new Date());
    <#elseif prop.javaType == 'String'>
		params.put("${prop.Name}", "String Value Updated");
    <#elseif prop.javaType == 'boolean'>
		params.put("${prop.Name}", true);
    <#elseif prop.javaType == 'int'>
		params.put("${prop.Name}", 2);
    <#elseif prop.javaType == 'long'>
		params.put("${prop.Name}", 2L);
    <#elseif prop.javaType == 'float'>
		params.put("${prop.Name}", 2.0F);
    <#elseif prop.javaType == 'double'>
		params.put("${prop.Name}", 2.0D);
    </#if>
    </#list>
		t.print(t.post("/${url}", params));
	} 

	/**
	 * test delete one row
	 */
	@Test
	public void testDelete0() {
		// TODO Modify Mock${ModelName}Mapper#delete To Complete Test Logic
		t.assertSuccess(t.print(t.delete("/${url}/1")));
	} 

	/**
	 * test delete multiple rows
	 */
	@Test
	public void testDelete1() {
		// TODO Modify Mock${ModelName}Mapper#delete To Complete Test Logic
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", new Object[] {1, 2, 3});
		t.assertSuccess(t.print(t.delete("/${url}", params)));
	} 
}
