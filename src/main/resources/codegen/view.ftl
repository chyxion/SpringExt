${objDoc}
Ext.define('App.views.${module?matches(r"\s*")?string('', module + '.')}${ModelName}.List', {
    extend: 'SpringExt.views.Grid',
    requires: ['Ext.ux.DataTip'],
    title: '${ModelName} List',
    selModel: Ext.create('Ext.selection.CheckboxModel', {
        mode: 'MULTI'
    }),
    paging_bar: true,
    store: Store.create({
        pageSize: 99,
        remoteSort: true,
        url: '${url}/list',
        fields: [
        'id', {
            name: 'dateCreated',
            type: 'date',
            dateFormat: 'time'
        }, {
            name: 'dateUpdated',
            type: 'date',
            dateFormat: 'time'
        }
    <#list cols as col>
        <#if col.javaType == 'Date'>
        , {
            name: '${col.name}',
            type: 'date',
            dateFormat: 'time'
        }
        <#else>
        , '${col.name}'
        </#if>
    </#list>    
        ],
        sorters: [{
        	property: 'dateCreated',
        	direction: 'ASC'
        }]
    }),
    columns: [{
        xtype: 'rownumberer'
    }, {
        dataIndex: 'id',
        text: 'ID',
        width: 128
    }, {
        dataIndex: 'dateCreated',
        text: 'Date Created',
        renderer: Utils.date_renderer(),
        width: 128
    },
    <#list cols as col>
    {
        <#if col.javaType == 'Date'>
        renderer: Utils.date_renderer(),
        width: 128,
        <#else>
        flex: 1,
        </#if>
        dataIndex: '${col.name}',
        text: '${col.Name}'
    },
    </#list>    
    {
        dataIndex: 'dateUpdated',
        text: 'Date Updated',
        renderer: Utils.date_renderer(),
        width: 128
    }],
    tbar: [{
    	text: 'Refresh',
        iconCls: 'refresh',
        handler: function (btn) {
            this.up('grid').getStore().loadPage(1);
        }
    }, {
        text: 'New ${ModelName}',
        iconCls: 'add',
        handler: function () {
            this.up('grid').show_dlg('post');
        }
    }, {
        text: 'Edit ${ModelName}',
        iconCls: 'edit',
        handler: function (btn) {
            var g = this.up('grid'),
                rec = g.getSelectionModel().getLastSelected();
            if (rec) {
                g.show_dlg('put', function() {
                    var f = this.down('form');
                    f.loadRecord(rec);
                });
            }
            else {
                Message.warn('Please Select ${ModelName} To Edit.');
            }
        }
    }, {
    	text: 'Remove',
        iconCls: 'remove',
        handler: function (btn) {
            var g = btn.up('grid'),
                s,
                ss = g.getSelectionModel().getSelection(),
                i,
                id;
            if (ss.length) {
                Dialog.confirm('Are You Sure To Remove The ${ModelName}(s) Selected?', function() {
                    s = g.getStore();
                    id = [];
                    // collect id
                    for (i = 0; i < ss.length; ++i) {
                        id.push(ss[i].get('id'));
                    }
                    Ajax.del('${url}', {
                        id: id
                    }, function() {
                        s.remove(ss);
                        Message.alert('${ModelName}(s) Removed Successfully.');
                    });
                });
            } 
            else {
                Message.warn('Please Select Row(s) To Remove.');
            }
        }
    }],
    show_dlg: function(type, fn) {
        var me = this,
            w, title, icon;
        if (type == 'put') {
            title = 'Edit ${ModelName}';
            icon = 'edit';
        }
        else {
            title = 'New ${ModelName}';
            icon = 'add';
        }
        w = Ext.create('Ext.Window', {
            title: title,
            iconCls: icon,
            closable: true,
            modal: true,
            plain: true,
            layout: 'fit',
            items: {
                xtype: 'form',
                layout: 'form',
                url: '${url}',
                bodyPadding: '12',
                border: 0,
                width: 350,
                defaultType: 'textfield',
                fieldDefaults: {
                    msgTarget: 'side',
                    allowBlank: false,
                    labelWidth: 75
                },
                plugins: {
                    ptype: 'datatip'
                },
                items: [{
                    name: 'id',
                    xtype: 'hidden'
                }
                <#list cols as col>
                , {
                    <#if col.javaType == 'Date'>
                    xtype: 'datefield',
                    submitFormat: 'Y-m-d H:i:s',
                    </#if>
                    <#if col.notNull>
                    afterLabelTextTpl: me.required,
                    allowBlank: false,
                    </#if>
                    fieldLabel: '${col.Name}',
                    name: '${col.name}'
                }
                </#list>
                ]
            },
            buttons: [{
                text: 'Save',
                iconCls: 'ok',
                handler: function() {
                    var w = this.up('window');
                    Ajax[type](w.down('form'), function() {
                        Message.alert('${ModelName} Saved Successfully.');
                        w.close();
                        me.getStore().load();
                    });
                }
            }, {
                text: 'Cancel',
                iconCls: 'cancel',
                handler: function() {
                    this.up('window').close();
                }
            }]
        });
        w.show(null, fn, w);
    }
});
