package ${pkg}.services;

import ${baseServiceFullName};
import ${pkg}.models.${ModelName};

${objDoc}
public interface ${ModelName}Service extends ${baseServiceName}<${ModelName}> {

}
