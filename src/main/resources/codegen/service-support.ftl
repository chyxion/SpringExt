package ${pkg}.services.support;

import org.springframework.stereotype.Service;
import ${baseServiceSupportFullName};
import ${pkg}.mappers.${ModelName}Mapper;
import ${pkg}.models.${ModelName};
import ${pkg}.services.${ModelName}Service;

${objDoc}
@Service
public class ${ModelName}ServiceSupport 
    extends ${baseServiceSupportName}<${ModelName}, ${ModelName}Mapper> 
    implements ${ModelName}Service {

}
