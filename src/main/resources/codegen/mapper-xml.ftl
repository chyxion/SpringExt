<?xml version="1.0" encoding="UTF-8"?>
<!--
${objDoc}
-->
<!DOCTYPE mapper PUBLIC 
	"-//mybatis.org//DTD Mapper 3.0//EN" 
	"http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${pkg}.mappers.${ModelName}Mapper">

	<resultMap id="modelMap" type="${ModelName}">
		<id column="id" property="id" />
		<result column="date_created" property="dateCreated" />
		<result column="date_updated" property="dateUpdated" />
		<#list cols as col>
		<result column="${col.col}" property="${col.name}" />
		</#list>
	</resultMap>

    <!-- variables -->    
	<sql id="columns">
	   id, date_created, date_updated
		<#list cols as col>
		  , ${col.col}
		</#list>
	</sql>
	<sql id="table">
	   ${table}
	</sql>
    <!-- /variables -->    

    <!-- methods -->
	<insert id="insert">
        <if test="models.length > 0">
            insert into <include refid="table" /> (
                <include refid="columns" />
            ) values 
            <foreach item="it" collection="models" separator=", ">
                (
                    ${'#'}{it.id}, ${'#'}{it.dateCreated}, ${'#'}{it.dateUpdated}
                    <#list cols as col>, ${'#'}{it.${col.name}}</#list>
                )
            </foreach>
        </if>
	</insert>

	<update id="update">
        <foreach item="it" collection="models" 
            separator="; ">
            update <include refid="table" />
            <set>
        <#list cols as col>
			<if test="it.${col.name} != null">
                ${col.col} = ${'#'}{it.${col.name}},
			</if>
        </#list>
            date_updated = ${'#'}{it.dateUpdated}
            </set>
            where id = ${'#'}{it.id}
        </foreach>
	</update>

	<delete id="delete">
		delete from <include refid="table" /> <include refid="Commons.idWhere" />
	</delete>

	<select id="find" resultMap="modelMap">
		select <include refid="columns" />
		from <include refid="table" />
        where id = ${'#'}{s}
	</select>
	
	<select id="list" resultMap="modelMap">
		select <include refid="columns" /> 
		from <include refid="table" /> <include refid="Commons.idWhere" />
	</select>

	<select id="listPage" resultMap="modelMap">
		select <include refid="columns" />
		from <include refid="table" />
	</select>
    <!-- /methods -->
</mapper>
