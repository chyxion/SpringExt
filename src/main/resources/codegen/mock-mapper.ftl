package ${pkg}.mappers.mock;

import java.util.List;
import java.util.Arrays;
import ${pageParamFullName};
import ${mockFullName};
import ${mockMapperFullName};
import ${pkg}.mappers.${ModelName}Mapper;
import ${pkg}.models.${ModelName};

${objDoc}
@${mockName}
public class Mock${ModelName}Mapper extends ${mockMapperName}<${ModelName}> implements ${ModelName}Mapper {

	/* (non-Javadoc)
	 * @see ${mockMapperFullName}#insert(java.lang.Object[])
	 */
	@Override
	public int insert(${ModelName} ... models) {
		// TODO Your Mock Codes Are Here
		return models.length;
	}

	/* (non-Javadoc)
	 * @see ${mockMapperFullName}#update(java.lang.Object[])
	 */
	@Override
	public int update(${ModelName} ... models) {
		// TODO Your Mock Codes Are Here
		return models.length;
	}

	/* (non-Javadoc)
	 * @see ${mockMapperFullName}#delete(java.lang.Object)
	 */
	@Override
	public int delete(Object search) {
		// TODO Your Mock Codes Are Here
		return 0;
	}

	/* (non-Javadoc)
	 * @see ${mockMapperFullName}#find(java.lang.Object)
	 */
	@Override
	public ${ModelName} find(Object search) {
		// TODO Your Mock Codes Are Here
		return new ${ModelName}();
	}

	/* (non-Javadoc)
	 * @see ${mockMapperFullName}#list(java.lang.Object)
	 */
	@Override
	public List<${ModelName}> list(Object search) {
		// TODO Your Mock Codes Are Here
		return Arrays.asList(new ${ModelName}());
	}

	/* (non-Javadoc)
	 * @see ${mockMapperFullName}#listPage(${pageParamFullName})
	 */
	@Override
	public List<${ModelName}> listPage(${pageParamName}<${ModelName}> pageParam) {
		// TODO Your Mock Codes Are Here
		return Arrays.asList(new ${ModelName}());
	}

	/* (non-Javadoc)
	 * @see ${mockMapperFullName}#listPageBy(${pageParamFullName}, java.lang.Object)
	 */
	@Override
	public List<${ModelName}> listPageBy(${pageParamName}<${ModelName}> pageParam, Object search) {
		// TODO Your Mock Codes Are Here
		return Arrays.asList(new ${ModelName}());
	}
}
