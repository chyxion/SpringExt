package ${pkg}.models;

import ${baseModelFullName};
<#list pkgs as p>
import ${p};
</#list>
<#if notNull>
import javax.validation.constraints.NotNull;
</#if>
<#if notBlank>
import org.hibernate.validator.constraints.NotBlank;
</#if>

${objDoc}
public class ${ModelName} extends ${baseModelName} {
    private static final long serialVersionUID = 1L;

<#list cols as prop>
    <#if prop.notNull>
    <#if prop.javaType == 'String'>
    ${'@NotBlank'}
    <#else>
    ${'@NotNull'}
    </#if>
    </#if>
	private ${prop.javaType} ${prop.name};
</#list>
<#list cols as prop>

	/**
	 * @return the ${prop.name}
	 */
    public ${prop.javaType} ${['boolean']?seq_contains(prop.javaType)?string('is', 'get')}${prop.Name}() {
		return ${prop.name};
	}

	/**
	 * @param ${prop.name} the ${prop.name} to set
	 */
	public void set${prop.Name}(${prop.javaType} ${prop.name}) {
		this.${prop.name} = ${prop.name};
	}
</#list>
}
