${objDoc}

create table ${table} (
    id varchar(36) not null,
    date_created datetime not null,
    date_updated datetime,
<#list cols as col>
    ${col.col} ${col.sqlType} ${col.notNull?string('not null', '')},
</#list>
    primary key (id)
);