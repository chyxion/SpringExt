<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<base href="${__BASE_PATH__}">
<link id="extjs_theme_styles" rel="stylesheet" href="assets/ext-js/themes/${RequestParameters.theme!'classic'}/styles.css" />
<link rel="stylesheet" href="assets/css/extjs-styles.css" />
<script src="assets/ext-js/ext-all${[true, 'true', '1']?seq_contains(RequestParameters.debug!false)?string('-debug', '')}.js"></script>
<script type="text/javascript">
    Ext.Loader.setConfig({
        enabled: true, 
        paths: {
            'Ext.ux': 'assets/ext-js/ux',
            'SpringExt': 'assets/js',
            'App': 'assets/js'
        }
    });
    Ext.BLANK_IMAGE_URL = 'assets/ext-js/s.gif';
</script>
<script src="assets/js/extjs-commons.js"></script>