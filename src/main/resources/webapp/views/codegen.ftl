<!DOCTYPE html>
<html>
<head>
    <#include "/commons/extjs-header.ftl" />
    <script type="text/javascript">
        /**
         * @version 1.0
         * @author Shaun Chyxion <br />
         * chyxion@163.com <br />
         * Dec 05, 2014 15:49:16 
         */
        Ext.onReady(function() {
            Ext.QuickTips.init();
            Ext.create('Ext.Viewport', {
                layout: 'border',
                items: [{
                        xtype: 'box',
                        region: 'north',
                        html: '<h1>Super Super Code!</h1>'
                    },
                    Ext.create('SpringExt.views.CodeGen', {
                        region: 'center'
                    })
                ]
            });
            Ext.create('SpringExt.views.ThemesBar').show();
        });
    </script>
    <title>Auto Code</title>
</head>
<body>
</body>
</html>
