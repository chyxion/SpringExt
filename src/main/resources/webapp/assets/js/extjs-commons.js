/*
 * @version 0.1
 * @author Shaun Chyxion
 * date created: 9/11/2012 9:49:18
 * support: chyxion@163.com
 * date modified: Nov 03, 2014 11:50:07
 */

var JSON;
JSON || (JSON = {
    // JSON.stringify(obj); // obj -> str
    stringify: Ext.JSON.encode,
    // JSON.parse(str); // str -> obj
    parse: Ext.JSON.decode
});
/**
 * URL params tool:
 * http://foobar.net/foo.html?bar=1&foobar=xyz
 * ->
 * params.base_path == 'http://foobar.net/'
 * params.bar == '1'
 * params.foobar == 'xyz'
 */
(function() {
    var w = window,
        l = w.location;
    w.console || (w.console = {log: function(){}, error: function(msg){
        alert(JSON.stringify(msg));
    }});
    w.params = {
        base_path: window.__base_path__ =
            l.protocol + '//' + l.host + '/' +
            l.pathname.split('/')[1] + '/'
    };
    // find params in url
    l.search.replace(/([^\?=\&]+)(=([^\&]*))?/g,
        function($0, $1, $2, $3){
            window.params[$1] = $3;
        }); // end of replace
})(); // end

/**
 * Ext Dialog
 */
var Dialog = {
    /**
     * @param {String} msg 
     * @param {Function} fn
     */
    alert: function(msg, fn) {
        Ext.MessageBox.show({
            title : 'Alert',
            msg : msg,
            autoWidth : true,
            minWidth : 200,
            maxWidth : 500,
            buttons : Ext.MessageBox.OK,
            icon : Ext.MessageBox.INFO,
            fn: fn
        });
    },
    /**
     * @param {String} msg
     * @param {Function} fn
     */
    warn: function(msg, fn) {
        Ext.MessageBox.show({
            title : 'Warn',
            msg : msg,
            autoWidth : true,
            minWidth : 200,
            maxWidth : 500,
            buttons : Ext.MessageBox.OK,
            icon : Ext.MessageBox.WARNING,
            fn: fn
        });
    },
    /**
     * @param {String} msg
     * @param {Function} fn_ok
     * @param {Function} fn_no
     */
    confirm: function(msg, fn_ok, fn_no) {
        Ext.MessageBox.confirm('Alert', msg || 'Are You Sure?', function(btn) {
            if (btn == 'yes') {
                fn_ok && fn_ok();
            }
            else {
                fn_no && fn_no();
            }
        });
    },
    /**
     * @param {String} title
     * @param {String} msg
     * @param {Function} fn_ok
     * @param {Function} fn_no
     */
    prompt: function(title, msg, fn_ok, fn_no) {
        Ext.MessageBox.prompt(title, msg, function(btn, text) {
            if (btn == 'ok') {
                fn_ok && fn_ok(text);
            }
            else {
                fn_no && fn_no();
            }
        });
    },
    error: function(msg) {
        Ext.MessageBox.show({
            title : 'Error',
            msg : msg || 'Error Caused.',
            autoWidth : true,
            minWidth : 200,
            maxWidth : 500,
            buttons : Ext.MessageBox.OK,
            icon : Ext.MessageBox.ERROR
        });
    },
    success : function(msg) {
        Ext.MessageBox.show({
            title: 'Success',
            msg: msg || 'Success!',
            autoWidth: true,
            minWidth: 200,
            maxWidth: 500,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO
        });
    },
    fail : function(msg) {
        Ext.MessageBox.show( {
            title: 'Fail',
            msg: msg || 'Operate Failed.',
            autoWidth: true,
            minWidth: 200,
            maxWidth: 500,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR
        });
    },
    waiting : function(msg) {
        Ext.MessageBox.show( {
            title : 'Waiting...',
            msg : msg || 'Please Wait...',
            width : 300,
            wait : true,
            waitConfig : {
                interval : 200
            },
            animEl : 'mb7'
        });
    },
    saving : function(msg) {
        Ext.MessageBox.show( {
            title: 'Save...',
            msg: msg || 'Saving, Please Wait...',
            width: 300,
            wait: true,
            waitConfig: {
                interval: 200
            },
            icon: 'ext-mb-download',
            animEl: 'mb7'
        });
    },
    loading : function(msg) {
        Ext.MessageBox.show( {
            title: 'Loading...',
            msg: msg || 'Loading, Please Wait...',
            width: 300,
            wait: true,
            icon: 'loading',
            waitConfig: {
                interval : 80
            },
            animEl : 'mb7'
        });
    },
    /**
     * hide dialog
     */
    hide: function() {
        Ext.MessageBox.hide();
    }
},
/**
 * Message box
 */
Message = {
    alert: function(title, msg) {
        if (!msg) {
            msg = title;
            title = 'Alert';
        }
        this._msg(title, msg, 'bg-alert');
    },
    warn: function(title, msg) {
        if (!msg) {
            msg = title;
            title = 'Warn';
        }
        this._msg(title, msg, 'bg-warn');
    },
    error: function(msg) {
         this._msg('Error', msg, 'bg-warn');
    },
    success: function(msg) {
         this._msg('Success', msg, 'bg-warn');
    },
    fail: function(msg) {
         this._msg('Faile', msg, 'bg-warn');
    },
    _create_box: function(t, s, cls) { // 创建提示框内容box
        return '<div class="msg ' + cls + '"><h3>' +
        t + '</h3><p>' +
        s + '</p></div>';
     },
    _msg: function(t, msg, type) {
        var me = this,
        // message div
        msg_div = Ext.get('commons-message-div') || 
        Ext.DomHelper.insertFirst(document.body, 
                {id: 'commons-message-div', style: 'z-index: 30240'});
        Ext.DomHelper.append(msg_div,
                me._create_box(t, msg, type), true)
            .hide()
            .slideIn('t')
            .ghost('t', {delay: 1500, remove: true});
    }
},
/**
 * Ajax
 * get:
 * Ajax.get('this/is/a/url', function(data){
 *  console.log(data.users);
 * });
 * Ajax.get('this/is/a/url', {
 *    user_id: '2008110101'
 *  }, function(user){
 *    console.log(user.name);
 * });
 * post:
 * Ajax.post('this/is/a/url', {
 *    user: {
 *        name: 'chyxion',
 *        id: '2008110101'
 *      };
 *  }, function(){
 *    Dialog.alert('保存成功!');
 * });
 * Ajax.post(form, function(){
 *    Dialog.alert('保存成功!');
 * });
 */
Ajax = {
    /**
     * private
     */
    _request: function(p) {
        // loading mask
        var me = this, lm;

        if (p.loadmask) {
            lm = Utils.loadmask(p.loadmask);
            lm.show();
        }

        p.before_load && p.before_load(); // 请求前回调
        // if not string, convert to json string
        if (p.params && !Ext.isFunction(p.params)) {
            var a, v;
            for (a in p.params) {
                v = p.params[a];
                Ext.isString(v) || (p.params[a] = JSON.stringify(v));
            }
        }
        else if (p.params && Ext.isFunction(p.params)) {
            p.params = p.params();
        }
        p.params || (p.params = {});
        // add _ajax param
        if (p.method != 'GET') {
            p.params._ajax = 1;
        }
        p.params._method = p.method;
        Ext.Ajax.request({
            method: p.method == 'GET' ? 'GET' : 'POST',
            url: p.url,
            params: p.params,
            disableCachingParam: '_ajax',
            disableCaching: true,
            success: function(r, opt) {
                p.loadmask && top.Ext.destroy(lm);
                p.post_request && p.post_request(r);
                try {
                    r = JSON.parse(r.responseText);
                }
                catch (e) {
                    // json parse exception
                    Dialog.warn('[' + r.responseText + '] is not valid JSON data.');
                    return false;
                }
                if (r.success) {
                    // success callback equals false, return
                    if (p.fn_s === false) return;
                    // call success callback if provided
                    Ext.isFunction(p.fn_s) ? p.fn_s(r.data, r) :
                        // use default_callback if exists
                        me.default_callback ? me.default_callback(r.data, r) :
                        // alert success message
                        Dialog.alert(r.data || 'Success.', r);
                }
                else {
                    // call fail callback if provided
                    p.fn_f ? p.fn_f(r.message || 'Error Caused.', r) :
                        // use default callback if exists
                        me.default_callback ? me.default_callback(r.data, r) :
                        // alert fail message
                        Dialog.alert('Error Caused, Message [' +
                                r.status.message + '], Code [' +
                                r.status.code + '].');
                }
            },
            failure : function(r, opt) {
                p.loadmask && top.Ext.destroy(lm);
                // lm.desroy(true);
                p.post_request && p.post_request(r);
                console.log ('ERROR: Net Connect Fail.');
            }
        });
    },
    _process_request: function(args, method) {
        var me = this;
        if (args && args.length) {
            // form submit
            if (Ext.Array.contains(['Ext.form.Panel', 'Ext.form.Basic'], 
                        Ext.getClassName(args[0]))) {
                var fn_s = args[1],
                    fn_f = args[2],
                    form = args[0].getForm ? args[0].getForm() : args[0];

                form.isValid() &&
                form.submit({
                    method: method == 'GET' ? 'GET' : 'POST',
                    params: {
                        _method: method,
                    },
                    success: function(form, action) {
                        fn_s ? fn_s(form, action) : 
                            Message.alert('Submit Success.');
                    },
                    failure: function(form, action) {
                        console.log('Form Submit ERROR, Net Connect Fail.');
                        fn_f ? fn_f(form, action) :
                            Dialog.alert('Error Caused, Message [' +
                                   action.result.status.message + 
                                   '], Code [' +
                                   action.result.status.code + '].');
                    }
                });
            }
            // ajax request
            else {
                // args to array
                args = Array.prototype.slice.call(args);
                var lm = args.shift(), // loadmask
                    url,
                    p,
                    fn_s,
                    fn_f;
                // loadmask
                if (Ext.isBoolean(lm)) {
                    url = args.shift();
                }
                else {
                    url = lm;
                    lm = 'Loading...'; // default loadmask
                }
                // request params
                p = args.shift();
                fn_s = args.shift();
                fn_f = args.shift();
                if (Ext.isFunction(fn_s) && Ext.isFunction(fn_f)) { // no request params found
                    Ext.isFunction(p) && (p = p());
                }
                else if (Ext.isFunction(p)) {
                    fn_f = fn_s;
                    fn_s = p;
                    p = null;
                }
                me._request({
                    loadmask: lm,
                    method: method || 'GET',
                    url: url,
                    params: p,
                    fn_s: fn_s,
                    fn_f: fn_f
                });
            }
        }
    },
    /**
     *  Ajax.get('url.get',
     *     {id: 001, name: 'foo'},
     *     function(data) {
     *       // success
     *     }, function(msg) {
     *       // fail
     *     });
     *     no params
     *  Ajax.get('url.get',
     *     function(data) {
     *       // success
     *     }, function(msg){
     *       // fail
     *     });
     * @param {Boolean} [loadmask], option
     * @param {String} url
     * @param {Object} [params]
     * @param {Function} [fn_s]
     * @param {Function} [fn_f]
     */
    get: function() {
        this._process_request(arguments, 'GET');
    },
    /**
     * @see #get
     */
    post: function() {
        this._process_request(arguments, 'POST');
    },
    /**
     * @see #get
     */
    put: function() {
        this._process_request(arguments, 'PUT');
    },
    /**
     * @see #get
     */
    del: function() {
        this._process_request(arguments, 'DELETE');
    },
    download: function(url, params) {
    	var di_id = '__download_iframe__',
    	di = Ext.get(di_id);
    	if (!di) {
    		di = Ext.DomHelper.append(document.body, {
    			tag: 'iframe',
    			id: di_id,
    			name: di_id,
    			width: 0,
    			height: 0,
    			style: 'display: none',
    			src: 'about:blank',
    		}, true);
    	}
    	di.set({src: params ? url + '?' + Ext.Object.toQueryString(params) : url});
    },
    load_script: function(url, fn_s, fn_f) {
        Ext.Loader.loadScript({
            url: url,
            onLoad: fn_s,
            onError: fn_f || function() {Dialog.warn('Load Script Fail.')}
        });
    }
},
Utils = {
    date_renderer: function(fmt) {
        return Ext.util.Format.dateRenderer(fmt || 'Y-m-d H:i:s');
    },
    cell_qt_renderer: function (v, md) {
        var e = Ext.String.htmlEncode;
        md.tdAttr = 'data-qtip="' + e(e(v)) + '"';
        return e(v);
    },
    page_size: 15,
    date_column_width: 136,
    loadmask: function (msg) { // 返回loading mask
        return new top.Ext.LoadMask(top.Ext.getBody(), {msg: msg || 'Loading...'});
    },
    is_ext_view: function (view) {
        return /^(\w+\.)+\w+$/.test(view);
    },
    /**
     * a=1,b=2 --> {a: 1, b: 2}
     */
    parse_args: function (args) {
        var r;
        if (Ext.isString(args)) {
            r = {};
            // remove blank char
            args = args.replace(/\s+/g, '');
            args.replace(/([^,=]+)(=([^,]+))?/g, function($0, $1, $2, $3) {
                    r[$1] = $3;
                }); // end of replace
        }
        else if (Ext.isObject(args)) {
            r = args;
        }
        return r;
    }
},
Store = {
    create: function (s) {
        var me = this,
        s_config = { // store config
            autoLoad: s.auto_load === undefined ? true : s.auto_load,
            store: 'json',
            pageSize: 50,
            proxy: me.ajax_proxy(s),
            listeners: me.listeners()
        };
        return Ext.create('Ext.data.Store', Ext.merge(s_config, s));
    },
    ajax_proxy: function (s) {
        return {
            type: 'ajax',
            cacheString: '__ajax',
            url: s.url,
            extraParams: s.params || s.extraParams,
            actionMethods: {
                read: s.method || 'GET'
            },
            reader: {
                type: 'json',
                root: 'data',
                idProperty: s.id_property || s.idProperty,
                totalProperty: 'total'
            },
            listeners: {
                exception: function (proxy, op) { // Store Load Exception
                    console.log ('Store Load Exception.');
                    console.log (op.responseText);
                    try {
                        if (op.responseText) {
                            op = JSON.parse(op.responseText);
                            Dialog.alert(('Error Caused, Message [' +
                                op.status.message + '], Code [' +
                                op.status.code + '].') ||
                                'Store Load Error, Unknow Exception.');
                        }
                    } 
                    catch (e) {
                       op.responseText &&
                        Dialog.alert(op.responseText || 'Store Load Exception.');
                    }
                }
            }
        };
    },
    listeners: function () {
        return {
            beforeload: function (s) {
                var p = s.getProxy().extraParams, i;
                if (p) {
                    for (i in p) {
                        p[i] != null &&
                            !Ext.isString(p[i]) &&
                            (p[i] = JSON.stringify(p[i]));
                    }
                }
            }
        };
    },
    tree: function (s) { // tree store
        var me = this,
        ts = {
            store: 'tree',
            proxy: me.ajax_proxy(s),
            listeners: me.listeners(),
            root: Ext.merge({
                expanded: true,
                id: s.root_id || 'root',
                text: s.root_text || 'root'
            }, s.root)
        };
        return Ext.merge(ts, s);
    }
};
