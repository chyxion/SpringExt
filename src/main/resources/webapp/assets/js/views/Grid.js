/**
 * Common grid
 * @version 0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * 3/19/2013 10:27:17 
 */
Ext.define('SpringExt.views.Grid', {
    extend:'Ext.grid.Panel',
    alias: 'widget.cgrid',
    columnLines: true,
    loadMask: true,
    required: '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>',
    selModel: {  // multiple selection
        mode: 'MULTI',
        allowDeselect: true
    },
    initComponent: function() {
        var me = this;
          // store
          me.store = me.get_store();
          if (me.paging_bar) {
        	  me.bbar = Ext.create('Ext.PagingToolbar', {
                  store: me.store,
                  displayInfo: true,
                  displayMsg: 'Displaying topics {0} - {1} of {2}',
                  emptyMsg: "No topics to display",
        	  });
          }
          me.callParent();
    },
    get_store: function() { // create store，private
        var me = this, s;
        if (!me._store) {
        	s = me.store || me.build_store();
        	me.paging && me.page_size && (s.pageSize = me.page_size);
        	me._store = s.isStore ?  s : Ext.create('Ext.data.Store', s);
        }
        return me._store;
    }
});
