/**
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Dec 17, 2014 13:34:42
 */
Ext.define('SpringExt.views.ThemesBar', {
    show: function() {
        // toolbar
        setTimeout(function() {
            var toolbar = Ext.widget({
                xtype: 'toolbar',
                border: true,
                rtl: false,
                floating: true,
                fixed: true,
                preventFocusOnActivate: true,
                draggable: {
                    constrain: true
                },
                items: [{
                    xtype: 'combo',
                    rtl: false,
                    width: 170,
                    labelWidth: 45,
                    fieldLabel: 'Theme',
                    labelStyle: 'cursor:move;',
                    margin: '0 5 0 0',
                    store: [
                        // ['access', 'access'], 
                        ['classic', 'classic'],
                        ['gray', 'gray'],
                        ['neptune', 'neptune']
                    ],
                    value: params.theme || 
                    	Ext.get('extjs_theme_styles')
                    		.getAttribute('href')
                    		.match(/\/themes\/([^/]+)\/styles\.css$/)[1],
                    listeners: {
                        change: function(cb, nv) {
                        	location.search = Ext.Object.toQueryString(
                        		Ext.apply(Ext.Object.fromQueryString(
                        			location.search), {theme: nv}));
                        }
                    }
                }, {
                    xtype: 'tool',
                    type: 'close',
                    rtl: false,
                    handler: function() {
                        this.up('toolbar').destroy();
                    }
                }],
                // Extra constraint margins within default constrain region of parentNode
                constraintInsets: '0 -' + (Ext.getScrollbarSize().width + 4) + ' 0 0'
            });
            toolbar.show();
            toolbar.alignTo(
                document.body,
                Ext.optionsToolbarAlign || 'tr-tr',
                [(Ext.getScrollbarSize().width + 4) * (Ext.rootHierarchyState.rtl ? 1 : -1),
                    -(document.body.scrollTop || document.documentElement.scrollTop)]
            );
            
            var constrainer = function() {
                toolbar.doConstrain();
            };
            
            Ext.EventManager.onWindowResize(constrainer);
            toolbar.on('destroy', function() { 
                Ext.EventManager.removeResizeListener(constrainer);
            });
        }, 100);
    }
});
