package me.chyxion.dao.datasource;

/**
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Sep 6, 2014 9:24:01 PM
 */
public class DataSourceHolder {

	private static ThreadLocal<String> dataSource = new ThreadLocal<String>();
	
	public static void set(String name) {
		dataSource.set(name);
	}
	
	public static String get() {
		return dataSource.get();
	}
	
	public static void remove() {
		dataSource.remove();
	}
}

