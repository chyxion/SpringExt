package me.chyxion.dao.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * Naming data source routing.
 * Using which data source by the name witch binded in the current thread.
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Sep 6, 2014 9:24:36 PM
 */
public class DataSourceRouting extends AbstractRoutingDataSource {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource#determineCurrentLookupKey()
	 */
	@Override
	protected Object determineCurrentLookupKey() {
		return DataSourceHolder.get();
	}
}