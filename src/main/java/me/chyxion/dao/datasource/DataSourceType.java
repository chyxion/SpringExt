package me.chyxion.dao.datasource;

/**
 * data source type constants
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Sep 6, 2014 9:25:45 PM
 */
public final class DataSourceType {
	/**
	 * Default Data Source Name
	 */
	public static final String DEFAULT = "DEFAULT";
	/**
	 * Main Data Source
	 */
	public static final String MAIN = "MAIN";
	/**
	 * Read Data Source Name
	 */
	public static final String READ = "READ";
	/**
	 * Write Data Source Name
	 */
	public static final String WRITE = "WRITE";
}