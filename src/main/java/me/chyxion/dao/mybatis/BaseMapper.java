package me.chyxion.dao.mybatis;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import me.chyxion.dao.mybatis.pagination.PageParam;

/**
 * @version 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Jun 8, 2014 4:11:38 PM
 */
public interface BaseMapper<T> {

	/**
	 * insert
	 * @param models
	 * @return
	 */
    int insert(@Param("models") T ... models);

    /**
     * update
     * @param models
     * @return
     */
    int update(@Param("models") T ... models);

    /**
     * delete model of id
     * <pre> 
     * 	mapper.delete("110101");
     * 	mapper.delete(new Object[] {"110101", "110102"});
     * 	mapper.delete(Arrays.asList("110101", "110102"));
     * </pre>
     * @param search id or array or list of id
     * @return
     */
    int delete(@Param("s") Object search);

    /**
     * find one
     * @param search
     * @return
     */
    T find(@Param("s") Object search);

    /**
     * list models of id
     * <pre> 
     * 	mapper.list("110101");
     * 	mapper.list(new Object[] {"110101", "110102"});
     * 	mapper.list(Arrays.asList("110101", "110102"));
     * </pre>
     * @param search array or collection of id
     * @return
     */
    List<T> list(@Param("s") Object search);

    /**
     * list page
     * <pre>
	 *	PageParam<User> pp = new PageParam<User>(0, 50);
	 * 	pp.addSort("dateCreated", "desc");
	 *	mapper.listPage(pp);
	 *	</pre.
     * @param pageParam
     * @return
     */
    List<T> listPage(
    		@Param("pageParam")
    		PageParam<T> pageParam);

    /**
     * search page
     * <pre>
	 *	PageParam<User> pp = new PageParam<User>(0, 50);
	 * 	pp.addSort("dateCreated", "desc");
	 *	mapper.listPageBy(pp, ...);
	 *	</pre.
     * @param pageParam 
     * @param search 
     * @return
     */
    List<T> listPageBy(
    		@Param("pageParam")
    		PageParam<T> pageParam, 
    		@Param("s")
    		Object search);
}
