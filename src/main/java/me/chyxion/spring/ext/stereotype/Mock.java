package me.chyxion.spring.ext.stereotype;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.stereotype.Component;

/**
 * @version 0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Jul 18, 2014 9:02:08 AM
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Mock {
	String value() default "";
}
