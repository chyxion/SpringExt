package me.chyxion.spring.ext.codegen.services.support;

import java.util.Map;
import me.chyxion.spring.ext.codegen.services.CodeGenerator;
import org.springframework.stereotype.Service;

/**
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Oct 7, 2014 7:52:21 PM
 */
@Service
public class MapperTestCodeGen extends CodeGenerator {

	/*
	 * (non-Javadoc)
	 * @see me.chyxion.spring.ext.codegen.services.CodeGenerator#process(java.util.Map, java.lang.String, java.lang.String)
	 */
    @Override
    public String process(Map<String, Object> dataModel, String module, String model) {
    	return render(testDir + dataModel.get("pkgDir") + 
    			"/mappers/" + model + "MapperTest.java",
    			"/codegen/mapper-test.ftl", dataModel);
    }
}
