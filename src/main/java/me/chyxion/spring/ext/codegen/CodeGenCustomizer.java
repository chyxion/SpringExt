package me.chyxion.spring.ext.codegen;

import java.util.Map;

/**
 * @version 0.0.1
 * @since 0.0.4
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Feb 4, 2015 4:42:01 PM
 */
public interface CodeGenCustomizer {

	/**
	 * customize your code gen result
	 * @param args
	 * @return
	 */
	void customize(CodeGenArgs args);

	class CodeGenArgs {
		/**
		 * result file
		 */
		private String file;
		/**
		 * free marker template
		 */
		private String ftl;
		/**
		 * free marker data model
		 */
		private Map<String, Object> model;

		/**
		 * @param file
		 * @param ftl
		 * @param model
		 */
		public CodeGenArgs(String file, String ftl, Map<String, Object> model) {
			this.file = file;
			this.ftl = ftl;
			this.model = model;
		}
		/**
		 * @return the file
		 */
		public String getFile() {
			return file;
		}
		/**
		 * @param file the file to set
		 */
		public void setFile(String file) {
			this.file = file;
		}
		/**
		 * @return the ftl
		 */
		public String getFtl() {
			return ftl;
		}
		/**
		 * @param ftl the ftl to set
		 */
		public void setFtl(String ftl) {
			this.ftl = ftl;
		}
		/**
		 * @return the model
		 */
		public Map<String, Object> getModel() {
			return model;
		}
		/**
		 * @param model the model to set
		 */
		public void setModel(Map<String, Object> model) {
			this.model = model;
		}
	}
}
