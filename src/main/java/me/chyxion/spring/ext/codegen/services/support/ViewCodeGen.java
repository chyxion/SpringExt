package me.chyxion.spring.ext.codegen.services.support;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import me.chyxion.spring.ext.codegen.services.CodeGenerator;

/**
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Oct 6, 2014 1:17:22 PM
 */
@Service
public class ViewCodeGen extends CodeGenerator {

	/*
	 * (non-Javadoc)
	 * @see me.chyxion.spring.ext.codegen.services.CodeGenerator#gen(java.util.Map, java.lang.String, java.lang.String)
	 */
    @Override
    public String process(Map<String, Object> dataModel, String module, String model) {
    	StringBuilder sbFilePath = new StringBuilder(viewsDir);
    	if (StringUtils.isNotBlank(module)) {
    		sbFilePath.append(module).append("/");
    	}
    	sbFilePath.append(model).append("/List.js");
    	return render(sbFilePath, "/codegen/view.ftl", dataModel);
    }
}
