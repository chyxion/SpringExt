package me.chyxion.spring.ext.codegen.services.support;

import java.util.Map;
import org.springframework.stereotype.Service;
import me.chyxion.spring.ext.codegen.services.CodeGenerator;

/**
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Oct 6, 2014 1:17:30 PM
 */
@Service
public class ServiceCodeGen extends CodeGenerator {

	/*
	 * (non-Javadoc)
	 * @see me.chyxion.spring.ext.codegen.services.CodeGenerator#process(java.util.Map, java.lang.String, java.lang.String)
	 */
    @Override
    public String process(Map<String, Object> dataModel, String module, String model) {
    	String pkgDir = (String) dataModel.get("pkgDir");
    	// interface
    	String strRtn = render(codeDir + pkgDir + 
    		"/services/" + model + "Service.java", 
    		"/codegen/service.ftl", dataModel);
    	// support
    	strRtn +=";";
    	strRtn += render(codeDir + pkgDir + 
    		"/services/support/" + model + "ServiceSupport.java", 
    		"/codegen/service-support.ftl", dataModel);
    	return strRtn;
    }
}
