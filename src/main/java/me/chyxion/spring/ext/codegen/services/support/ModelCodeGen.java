package me.chyxion.spring.ext.codegen.services.support;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import me.chyxion.spring.ext.codegen.services.CodeGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Oct 6, 2014 1:17:42 PM
 */
@Service
public class ModelCodeGen extends CodeGenerator {
	private static final Map<String, String> TYPE_PACKAGE = new HashMap<String, String>() {
        private static final long serialVersionUID = 1L;
        {
        	put("Date", "java.util.Date");
        }
	};

	/*
	 * (non-Javadoc)
	 * @see me.chyxion.spring.ext.codegen.services.CodeGenerator#process(java.util.Map, java.lang.String, java.lang.String)
	 */
    @Override
    @SuppressWarnings("unchecked")
    public String process(Map<String, Object> dataModel, String module, String model) {
	    
    	Set<String> pkgs = new HashSet<String>();
    	for (Map<String, Object> col : (List<Map<String, Object>>) dataModel.get("cols")) {
    		String p = TYPE_PACKAGE.get(col.get("javaType"));
    		if (StringUtils.isNotBlank(p)) {
    			pkgs.add(p);
    		}
        }
    	dataModel.put("pkgs", pkgs);

    	return render(codeDir + dataModel.get("pkgDir") + 
    		"/models/" + model + ".java", 
    		"/codegen/model.ftl", dataModel);
    }
}
