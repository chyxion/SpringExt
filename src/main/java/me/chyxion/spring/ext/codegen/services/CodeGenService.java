package me.chyxion.spring.ext.codegen.services;

import java.util.List;

/**
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Oct 6, 2014 1:08:14 PM
 */
public interface CodeGenService {
	/**
	 * process code generation
	 * @param module
	 * @param model
	 * @param cols
	 * @param table
	 * @param createTable
	 */
	void process(String module, String model, List<?> cols, String table, boolean createTable);

	/**
	 * list generated items
	 * @return items list
	 */
	List<?> list();

	/**
	 * delete items
	 * @param items
	 */
	void delete(List<?> items, boolean dropTable);
}
