package me.chyxion.spring.ext;

/**
 * for custom data model assemble
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Jan 19, 2015 4:54:55 PM
 */
public interface DataModelAssembler {

	/**
	 * custom json data model assemble, return your custom data model
	 * @param model
	 * @return
	 */
	Object assemble(DataModel model);
}