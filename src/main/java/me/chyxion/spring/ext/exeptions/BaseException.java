package me.chyxion.spring.ext.exeptions;

/**
 * @version 0.0.2
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Jul 18, 2014 9:02:52 AM
 */
public class BaseException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public static final int CODE = 5000;
    private Object code = CODE;

    /**
     * @param code
     */
    public BaseException(Object code, String message) {
        super(message);
        this.code = code;
    }

    public BaseException(Object code, String message, Throwable e) {
        super(message, e);
        this.code = code;
    }

    /**
     * @return the code
     */
    public Object getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public BaseException setCode(Object code) {
        this.code = code;
        return this;
    }
}
