package me.chyxion.spring.ext.exeptions;

import me.chyxion.spring.ext.DataModel;

/**
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Feb 3, 2015 12:56:38 PM
 */
public class BaseExceptionResolver implements ExceptionResolver {

	/* (non-Javadoc)
	 * @see me.chyxion.spring.ext.exeptions.ExceptionResolver#accept(java.lang.Throwable)
	 */
	@Override
	public boolean accept(Throwable ex) {
		return ex instanceof BaseException;
	}

	/* (non-Javadoc)
	 * @see me.chyxion.spring.ext.exeptions.ExceptionResolver#process(java.lang.Throwable)
	 */
	@Override
	public DataModel process(DataModel model) {
		BaseException be = (BaseException) model.getException();
		model.setCode(be.getCode());
		model.setMessage(be.getMessage());
		return model;
	}
}
