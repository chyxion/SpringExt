package me.chyxion.spring.ext.exeptions;

/**
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Jun 18, 2014 9:20:10 PM
 */
public class InvalidParamException extends BaseException {
	private static final long serialVersionUID = 1L;
	public static final int CODE = 4001;
	/**
	 * @param code
	 * @param message
	 */
	public InvalidParamException(String message) {
		super(CODE, message);
	}
}
