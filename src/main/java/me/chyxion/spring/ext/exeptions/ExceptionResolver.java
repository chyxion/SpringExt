package me.chyxion.spring.ext.exeptions;

import me.chyxion.spring.ext.DataModel;

/**
 * @version 0.0.1
 * @since 0.0.3
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Feb 3, 2015 11:27:31 AM
 */
public interface ExceptionResolver {

	/**
	 * decide exception to process
	 * @param ex
	 * @return
	 */
	boolean accept(Throwable ex);

	/**
	 * process model
	 * @param model
	 * @return
	 */
	DataModel process(DataModel model);
}
