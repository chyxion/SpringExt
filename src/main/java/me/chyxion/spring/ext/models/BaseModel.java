package me.chyxion.spring.ext.models;

import java.io.Serializable;
import java.util.Date;

/**
 * @version 0.0.2
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Oct 6, 2014 2:35:15 PM
 */
public class BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private Object id;
	private Date dateCreated;
	private Date dateUpdated;

	/**
	 * @return the id
	 */
	@SuppressWarnings("unchecked")
	public <T> T getId() {
		return (T) id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Object id) {
		this.id = id;
	}

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the dateUpdated
	 */
	public Date getDateUpdated() {
		return dateUpdated;
	}

	/**
	 * @param dateUpdated the dateUpdated to set
	 */
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
}
