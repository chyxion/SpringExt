package me.chyxion.spring.ext;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractRefreshableApplicationContext;

/**
 * Disable Bean Definition Override
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Jan 23, 2015 12:19:07 PM
 */
public class AppContextInitializer 
	implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextInitializer#initialize(org.springframework.context.ConfigurableApplicationContext)
	 */
	@Override
	public void initialize(ConfigurableApplicationContext appContext) {
		if (appContext instanceof AbstractRefreshableApplicationContext) {
            ((AbstractRefreshableApplicationContext) appContext)
            	.setAllowBeanDefinitionOverriding(false);
       }
	}
}
