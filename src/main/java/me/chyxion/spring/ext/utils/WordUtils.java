package me.chyxion.spring.ext.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Oct 31, 2014 1:27:57 PM
 */
public class WordUtils {

	/**
	 * split Camel Case
	 * <pre>
	 * nice -> [nice]
     * World -> [World]
     * MySQL -> [My SQL]
     * HTML -> [HTML]
     * JSONObject -> [JSON Object]
     * JPanel -> [J Panel]
     * toJSONString -> [to JSON String]
     * Log4J -> [Log 4 J]
     * Log4j -> [Log 4 j]
     * 99Roses -> [99 Roses]
     * Varchar2 -> [Varchar 2]
     * DO178 -> [DO 178]
     * Do178 -> [Do 178]
	 * </pre>
	 * @param str
	 * @return
	 */
	public static String[] splitCamelCase(String str) {
		return StringUtils.isNotBlank(str) ? 
					// JSONObject -> JSON Object
				str.split(new StringBuilder("(?<=[A-Z])(?=[A-Z][a-z])|") 
					// MySQL -> My SQL | Log4J -> Log 4 J
					.append("(?<=[^A-Z])(?=[A-Z])|") 
					// 5s -> 5 s
					.append("(?<=[^a-zA-Z])(?=[a-z])|") 
					// A3 -> A 3 | a3 -> a 3
					.append("(?<=[A-Za-z])(?=[^A-Za-z])") 
					.toString()) : 
				new String[]{};
	}

	/**
	 * Join Camel Case Word
	 * <pre>
	 * #join("nice", "_") -> [nice]
     * #join("World", "_") -> [World]
     * #join("MySQL", "_") -> [My_SQL]
     * #join("HTML", "_") -> [HTML]
     * #join("JSONObject", "_") -> [JSON_Object]
     * #join("JPanel", "_") -> [J_Panel]
     * #join("toJSONString", "_") -> [to_JSON_String]
     * #join("Log4J", "_") -> [Log_4_J]
     * #join("Log4j", "_") -> [Log_4_j]
     * #join("99Roses", "_") -> [99_Roses]
     * #join("Varchar2", "_") -> [Varchar_2]
     * #join("DO178", "_") -> [DO_178]
     * #join("Do178", "_") -> [Do_178]
     * #join("Do178", "-") -> [Do-178]
     * ...
	 * </pre>
	 * @param str
	 * @param separator
	 * @return
	 */
	public static String joinCamelCase(String str, String separator) {
    	return StringUtils.join(splitCamelCase(str), separator);
	}
}
