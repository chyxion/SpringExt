package me.chyxion.spring.ext.utils;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

/**
 * @version 1.0.0
 * @since 0.0.1
 * @author chyxion <br />
 * chyxion@163.com <br />
 * Jan 16, 2014 11:22:50 AM
 */
public class ResponseTool {
	private static final Logger log = 
			LoggerFactory.getLogger(ResponseTool.class);

	public static final String MARK_DATA = "__DATA__";
	public static final String MARK_RAW = "__RAW__";
	public static final String PREFIX_DATA = "data:";
	public static final String PREFIX_RAW0 = "raw:";
	public static final String PREFIX_REDIRECT = "redirect:";
	public static final String PREFIX_FORWORD = "forword:";
	public static final String PREFIX_JSP = "jsp:";
	public static final String PREFIX_FTL = "ftl:";

    public String data(CharSequence data) {
    	log.debug("Build Data Response [{}:{}].", PREFIX_DATA, data);
    	return PREFIX_DATA + data;
    }

    /**
     * Mark Map Return Render Raw
     * @param data
     * @return
     */
    public Map<String, ?> raw(Map<String, ? super Object> data) {
    	data.put(MARK_RAW, true);
    	return data;
    }

    /**
     * Return JSP View
     * @param jsp
     * @return
     */
    public String jsp(CharSequence jsp) {
    	log.debug("Build JSP Response [{}:{}].", PREFIX_JSP, jsp);
    	return PREFIX_JSP + jsp;
    }

    /**
     * Return JSP Model And View
     * @param mv
     * @return
     */
    public ModelAndView jsp(ModelAndView mv) {
    	mv.setViewName(PREFIX_JSP + mv.getViewName());
    	return mv;
    }

    /**
     * Return FreeMarker Model And View
     * @param mv
     * @return
     */
    public ModelAndView ftl(ModelAndView mv) {
    	mv.setViewName(PREFIX_FTL + mv.getViewName());
    	return mv;
    }

    /**
     * Return FreeMarker View
     * @param ftl
     * @return
     */
    public String ftl(CharSequence ftl) {
    	log.debug("Build FreeMarker Response [{}:{}].", PREFIX_FTL, ftl);
    	return PREFIX_FTL + ftl;
    }

    /**
     * Redirect To
     * @param redirect
     * @return
     */
    public String redirect(String redirect) {
    	log.debug("Build Redirect Response [{}:{}].", PREFIX_REDIRECT, redirect);
    	return PREFIX_REDIRECT + redirect;
    }

    /**
     * Forward To
     * @param forward
     * @return
     */
    public String forward(String forward) {
    	log.debug("Build Forward Response [{}:{}].", PREFIX_FORWORD, forward);
    	return PREFIX_FORWORD + forward;
    }
}
