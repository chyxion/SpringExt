package me.chyxion.spring.ext.utils;

import org.springframework.util.AlternativeJdkIdGenerator;

/**
 * @version 0.0.2
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Jun 12, 2014 9:47:28 PM
 */
public final class UUID {
	public static String get() {
		return new AlternativeJdkIdGenerator().generateId().toString();
	}
}