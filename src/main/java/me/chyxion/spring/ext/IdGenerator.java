package me.chyxion.spring.ext;

/**
 * @version 0.0.2
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Jan 20, 2015 4:01:51 PM
 */
public interface IdGenerator<T> {
	T get();
}
