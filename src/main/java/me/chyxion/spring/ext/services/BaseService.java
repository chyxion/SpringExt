package me.chyxion.spring.ext.services;

import java.util.List;
import java.util.Map;
import me.chyxion.dao.datasource.DataSource;
import me.chyxion.dao.datasource.DataSourceType;
import me.chyxion.spring.ext.models.BaseModel;

/**
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Oct 6, 2014 2:20:58 PM
 */
public interface BaseService<T extends BaseModel> {

	/**
	 * find model
	 * @param search
	 * @return
	 */
	@DataSource(DataSourceType.READ)
	T find(Object search);

	/**
	 * list data
	 * @param start
	 * @param limit
	 * @param sort
	 * @return
	 */
	@DataSource(DataSourceType.READ)
	Map<String, Object> list(int start, int limit, List<?> sort);

	/**
	 * create models
	 * @param models
	 * @return
	 */
	@DataSource(DataSourceType.WRITE)
	T[] create(T ... model);

	/**
	 * update models
	 * @param models
	 * @return
	 */
	@DataSource(DataSourceType.WRITE)
	T[] update(T ... models);

	/**
	 * delete model
	 * @param id id list
	 */
	@DataSource(DataSourceType.WRITE)
	int delete(List<?> id);
	
	/**
	 * @since 0.0.2
	 * delete modeles
	 * @param id id array
	 */
	@DataSource(DataSourceType.WRITE)
	int delete(Object ... id);
}