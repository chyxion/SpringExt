package me.chyxion.spring.ext.view;

import java.util.Locale;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

/**
 * Remove View Name Mark
 * @version 0.0.2
 * @since 0.0.1
 * @author chyxion <br />
 * chyxion@163.com <br />
 * Dec 5, 2014 2:31:39 PM
 */
public class FreeMarkerViewResolverExt extends FreeMarkerViewResolver {

    @Override
    public View resolveViewName(String viewName, Locale locale) 
    		throws Exception {
    	return super.resolveViewName(viewName.replaceAll("(?i)(^\\s*ftl\\:)|(\\.ftl\\s*$)", ""), locale);
    }
}
