package me.chyxion.spring.ext.test;

import java.util.List;
import me.chyxion.dao.mybatis.BaseMapper;
import me.chyxion.dao.mybatis.pagination.PageParam;

/**
 * @version 0.0.1
 * @since 0.0.1
 * @author Shaun Chyxion <br />
 * chyxion@163.com <br />
 * Oct 6, 2014 5:08:43 PM
 */
public class MockMapper<T> implements BaseMapper<T> {

	/* (non-Javadoc)
	 * @see me.chyxion.dao.mybatis.BaseMapper#insert(T[])
	 */
    @Override
    public int insert(T ... models) {
	    return models.length;
    }

	/* (non-Javadoc)
	 * @see me.chyxion.dao.mybatis.BaseMapper#update(T[])
	 */
    @Override
    public int update(T ... models) {
	    return models.length;
    }

	/* (non-Javadoc)
	 * @see me.chyxion.dao.mybatis.BaseMapper#delete(java.lang.Object)
	 */
    @Override
    public int delete(Object search) {
	    return 0;
    }

	/* (non-Javadoc)
	 * @see me.chyxion.dao.mybatis.BaseMapper#find(java.lang.Object)
	 */
    @Override
    public T find(Object search) {
	    return null;
    }

	/* (non-Javadoc)
	 * @see me.chyxion.dao.mybatis.BaseMapper#list(java.lang.Object)
	 */
    @Override
    public List<T> list(Object search) {
	    // TODO Auto-generated method stub
	    return null;
    }

	/* (non-Javadoc)
	 * @see me.chyxion.dao.mybatis.BaseMapper#listPage(me.chyxion.dao.mybatis.pagination.PageParam)
	 */
    @Override
    public List<T> listPage(PageParam<T> pageParam) {
	    // TODO Auto-generated method stub
	    return null;
    }

	/* (non-Javadoc)
	 * @see me.chyxion.dao.mybatis.BaseMapper#listPageBy(me.chyxion.dao.mybatis.pagination.PageParam, java.lang.Object)
	 */
    @Override
    public List<T> listPageBy(PageParam<T> pageParam, Object search) {
	    return null;
    }
}
